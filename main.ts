import {Observable, onErrorResumeNext, fromEvent, merge, of, from, throwError, defer} from 'rxjs';
import {retryWhen, scan, takeWhile, map, flatMap, catchError,
    filter, delay} from 'rxjs/operators';
import {load, loadWithFetch} from './loader';

let output = document.querySelector('#output');
let button = document.querySelector('#button');

let click = fromEvent(button, 'click');

function renderMovies(movies) {
    movies.forEach(movie => {
        let div = document.createElement('div');
        div.innerText = movie.title;
        output.appendChild(div);
    });
}

let subsrciption =
    load('-movies.json')
        .subscribe(renderMovies,
            error => console.log(`caught error: ${error}`),
            () => console.log('completed!')
)

click.pipe(flatMap(e => loadWithFetch('movies.json')))
    .subscribe(
        renderMovies,
        error => console.log(`error: ${error}`),
        () => console.log('completed!')
)
