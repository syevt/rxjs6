module.exports = {
    entry: './main', //will look for main.ts (js or no ext)
    output: {filename: 'app.js'}, // bundled js file
    module: {
        rules: [
            {
                test: /.ts$/,
                loader: 'ts-loader' // this was installed as dev dep previously
            }
        ]
    },
    resolve: {
        extensions: ['*', '.ts', '.js'] // which exts to look for
    }
}
