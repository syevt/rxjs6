import {Observable, fromEvent, from, defer} from 'rxjs';
import {retryWhen, scan, takeWhile, delay} from 'rxjs/operators';

export function load (url) {
    return Observable.create(observer => {
        let xhr = new XMLHttpRequest();

        let onLoad = () => {
            if(xhr.status === 200) {
                let data = JSON.parse(xhr.responseText);
                observer.next(data);
                observer.complete();
            } else {
                observer.error(xhr.statusText)
            }
        }

        xhr.addEventListener('load', onLoad);

        xhr.open('GET', url);
        xhr.send();

        return () => {
            xhr.removeEventListener('load', onLoad);
            xhr.abort();
        }
    }).pipe(retryWhen(retryStrategy({attempts: 10, wait: 500})));
}

export function loadWithFetch(url) {
    return defer(() => {
        return from(fetch(url).then(r => {
            if (r.status === 200) {
                return r.json();
            } else {
                return Promise.reject(r);
            }
        }))
    }).pipe(retryWhen(retryStrategy()));
}

function retryStrategy({attempts = 4, wait = 1000} = {}) {
    return function(errors) {
        return errors.pipe(
                 scan((acc, value) => {
                     acc += 1;

                     if (acc < attempts) {
                         return acc;
                     } else {
                         throw new Error(String(value));
                     }
                 }, 0),
                 delay(wait)
        );
    }
}
